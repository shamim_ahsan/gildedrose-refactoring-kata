﻿using NUnit.Framework;
using System.Collections.Generic;
using csharp.constants;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        [Test]
        public void foo()
        {
            IList<Item> items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(items);
            app.UpdateQuality();
            Assert.AreEqual("foo", items[0].Name);
        }
        [Test]
        public void AgedBrie()
        {
            IList<Item> items = new List<Item> { new Item { Name = ItemNameConstant.AgedBrie, SellIn = 2, Quality = 0 } };
            GildedRose app = new GildedRose(items);
            app.UpdateQuality();
            Assert.AreEqual(ItemNameConstant.AgedBrie, items[0].Name);
            Assert.AreEqual(1, items[0].Quality);
        }
        [Test]
        public void BackstagePassesToATafkal80EtcConcert()
        {
            IList<Item> items = new List<Item> { new Item { Name = ItemNameConstant.BackstagePassesToATafkal80EtcConcert, SellIn = 7, Quality = 0 } };
            GildedRose app = new GildedRose(items);
            app.UpdateQuality();
            Assert.AreEqual(ItemNameConstant.BackstagePassesToATafkal80EtcConcert, items[0].Name);
            Assert.AreEqual(2, items[0].Quality);
        }
        [Test]
        public void Conjured()
        {
            IList<Item> items = new List<Item> { new Item { Name = ItemNameConstant.Conjured, SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(items);
            app.UpdateQuality();
            Assert.AreEqual(ItemNameConstant.Conjured, items[0].Name);
            Assert.AreNotEqual(2, items[0].Quality);
        }
        [Test]
        public void SulfurasHandOfRagnaros()
        {
            IList<Item> items = new List<Item> { new Item { Name = ItemNameConstant.SulfurasHandOfRagnaros, SellIn = 20, Quality = 80 } };
            GildedRose app = new GildedRose(items);
            app.UpdateQuality();
            Assert.AreEqual(ItemNameConstant.SulfurasHandOfRagnaros, items[0].Name);
            Assert.AreEqual(80, items[0].Quality);
            Assert.AreEqual(20, items[0].SellIn);
        }
       
    }
}
