﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp.interfaces;

namespace csharp.models
{
    public class BackstagePassesToATafkal80EtcConcert:Item, IUpdater
    {
        public BackstagePassesToATafkal80EtcConcert(string name, int sellIn, int quality)
        {
            this.Name = name;
            this.SellIn = sellIn;
            this.Quality = quality;
        }

        public void UpdateSellIn()
        {
            this.SellIn = this.SellIn - 1;
        }

        public void UpdateQuality()
        {
            if (!(this.Quality < 50))
            {
                return; 
            }
            this.Quality = this.Quality + 1; 

            if (this.SellIn < 11 && this.Quality < 50)
            {
                this.Quality = this.Quality + 1;
            }

            if (this.SellIn < 6 && this.Quality < 50)
            {
                this.Quality = this.Quality + 1;
            }
            if (this.SellIn < 0)
            {
                this.Quality = this.Quality - this.Quality;
            }
        }
    }
}
