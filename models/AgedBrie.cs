﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp.interfaces;

namespace csharp.models
{
    public class AgedBrie:Item, IUpdater
    {
        public AgedBrie(string name, int sellIn, int quality)
        {
            this.Name = name;
            this.SellIn = sellIn;
            this.Quality = quality;
        }

        public void UpdateSellIn()
        {
            this.SellIn = this.SellIn - 1;
        }

        public void UpdateQuality()
        {
            if (this.Quality < 50)
            {
                this.Quality = this.Quality + 1;
            }

            if (this.SellIn < 0)
            {
                this.Quality = this.Quality + 1;
            }
        }
    }
}
