﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp.interfaces;

namespace csharp.models
{
    public class SulfurasHandOfRagnaros:Item, IUpdater
    {
        public SulfurasHandOfRagnaros(string name, int sellIn, int quality)
        {
            this.Name = name;
            this.SellIn = sellIn;
            this.Quality = quality;
        }

        public void UpdateSellIn()
        {
           
        }

        public void UpdateQuality()
        {
           
        }
    }
}
