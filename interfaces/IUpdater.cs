﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.interfaces
{
    public interface IUpdater
    {
        void UpdateSellIn();
        void UpdateQuality();
    }
}
