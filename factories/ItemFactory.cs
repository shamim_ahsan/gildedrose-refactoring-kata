﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp.constants;
using csharp.models;

namespace csharp.factories
{
    public class ItemFactory
    {
        public static Item GetItem(string name, int sellIn, int quality)
        {
            switch (name)
            {
                case ItemNameConstant.AgedBrie:
                    return new AgedBrie(name,sellIn,quality);
                case ItemNameConstant.BackstagePassesToATafkal80EtcConcert:
                    return new BackstagePassesToATafkal80EtcConcert(name, sellIn, quality);
                case ItemNameConstant.SulfurasHandOfRagnaros:
                    return new SulfurasHandOfRagnaros(name, sellIn, quality);
                case ItemNameConstant.Conjured:
                    return new Conjured(name, sellIn, quality);
                default:
                    return new DefaultItem(name, sellIn, quality);
            }
        }
    }
}
