﻿using System.Collections.Generic;
using csharp.constants;
using csharp.factories;
using csharp.interfaces;

namespace csharp
{
    public class GildedRose
    {
        IList<Item> Items;
        IList<Item> Fitems;
        public GildedRose(IList<Item> items)
        {
            Fitems = new List<Item>();
            foreach (var item in items)
            {
                Fitems.Add(ItemFactory.GetItem(item.Name,item.SellIn,item.Quality));
            } 
            this.Items = items;
        }
        // Seperated All 3 special Item Logic in every sub method (AgedBrie,BackstagePassesToATafkal80EtcConcert,SulfurasHandOfRagnaros)
        // And Normal Item
        public void UpdateQuality()
        {
            foreach (Item item in Fitems)
            {
                ((IUpdater)item).UpdateSellIn();
                ((IUpdater)item).UpdateQuality();
                //if (item.Name != ItemNameConstant.AgedBrie && item.Name != ItemNameConstant.BackstagePassesToATafkal80EtcConcert && item.Name != ItemNameConstant.SulfurasHandOfRagnaros)
                //{
                //    // Normal Item
                //    //if (item.Quality > 0)
                //    //{
                //    //    item.Quality = item.Quality - 1;
                //    //}
                //}
                //else
                //{
                //    // 3 constant name so this block for that 3 special item
                //    ProcessSpecialItemIncreaseQuality(item);
                //}

                //ProcessSellInExpiration(item);
            }

            for (int i = 0; i < Fitems.Count; i++)
            {
                Items[i].Quality = Fitems[i].Quality;
                Items[i].SellIn = Fitems[i].SellIn;
            }
        }

        //private static void ProcessSpecialItemIncreaseQuality(Item item)
        //{
        //    if (!(item.Quality < 50))
        //    {
        //        return; //add guard clause  SulfurasHandOfRagnaros also returned coz quality 80
        //    }

        //    if (item.Name == ItemNameConstant.BackstagePassesToATafkal80EtcConcert)
        //    {
        //        //item.Quality = item.Quality + 1; //add this to make guard clause 

        //        //if (item.SellIn < 11 && item.Quality < 50)
        //        //{
        //        //    item.Quality = item.Quality + 1;
        //        //}

        //        //if (item.SellIn < 6 && item.Quality < 50)
        //        //{
        //        //    item.Quality = item.Quality + 1;
        //        //}

        //        return; //add guard clause for BackstagePassesToATafkal80EtcConcert only
        //    }

        //    /*item.Quality = item.Quality + 1;*/ //Only For Special AgedBrie item



        //}

        //private static void ProcessSellInExpiration(Item item)
        //{
        //    if (item.Name == ItemNameConstant.SulfurasHandOfRagnaros) // Special Item 1 SulfurasHandOfRagnaros
        //    {
        //        return; //- "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
        //    }

        //    item.SellIn = item.SellIn - 1; //All item except Sulfuras

        //    if (item.SellIn < 0)
        //    {
        //        if (item.Name == ItemNameConstant.AgedBrie)// Special Item 2 AgedBrie
        //        {
        //            //if (item.Quality < 50)
        //            //{
        //            //    item.Quality = item.Quality + 1;
        //            //}

        //        }
        //        else
        //        {

        //            if (item.Name == ItemNameConstant.BackstagePassesToATafkal80EtcConcert)// Special Item 3 BackstagePassesToATafkal80EtcConcert
        //            {
        //                //item.Quality = item.Quality - item.Quality;
        //            }
        //            else
        //            {
        //                //if (item.Quality > 0)
        //                //{
        //                //    item.Quality = item.Quality - 1; // remove SulfurasHandOfRagnaros condition guard clause do the job
        //                //}
                       
        //            }
        //        }
        //    }
        //}
    }
}
