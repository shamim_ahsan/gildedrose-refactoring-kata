# README #

I think my best read me file content is my git commit message  

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Initial commit
* Add GildedRose-Refactoring-Kata C# In Repository
* Introduce constants & Variables
* Try to divide long method to sub independent method
* Introduce ProcessIncreaseQuality,ProcessSellInExpiration
* Simplify Conditions & Separate Special Item & Normal Item
* Add comment For Understanding
* Refactor Special Item Increase Method
* Simplify Conditions
* Add method Guard clause/Base Case
* remove nested conditions
* Flip Negative conditions to Positive Condition & Add Guard Clause
* Again Flip Conditions to Find Possible Cases & More Readability
* Seperated All 3 special Item Logic in every sub method (AgedBrie,BackstagePassesToATafkal80EtcConcert,SulfurasHandOfRagnaros)
* And Normal Item
* Added 4 Classes For Moving 3 Special Item Logic
* create Factory to get Typed Item
* Add Interface for common 2 property update
* moved default Item Logic in class
* moved default AgedBrie logic into class
* move BackstagePassesToATafkal80EtcConcert method logic into class
* main refactoring done
* "Conjured" requirement added to the system


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact